<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = ['id','word', 'word_compare', 'anagrams'];

    use HasFactory;
    protected $table = 'work';

}
